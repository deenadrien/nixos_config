{ pkgs ? import <nixpkgs> {} }:

pkgs.python3Packages.buildPythonPackage {
  name = "py_randmat3";
  src = ./.;
  propagatedBuildInputs = [
    pkgs.python3Packages.numpy
  ];
}

