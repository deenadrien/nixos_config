from setuptools import setup

setup(name='py_randmat3',
      version='0.1.0',
      scripts=['py_randmat3.py'],
      install_requires=['numpy'])

