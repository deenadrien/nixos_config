with import <nixpkgs> {};

pkgs.stdenv.mkDerivation {
  name = "hs-randmat2";
  buildInputs = [ 
    (haskellPackages.ghcWithPackages (ps: with ps; [
      random
    ]))
  ];
}

