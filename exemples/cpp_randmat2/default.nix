with import<nixpkgs> {};

stdenv.mkDerivation {
  name = "cpp_randmat2";
  src = ./.;

  buildInputs = [
    pkgconfig
    eigen3_3
  ];

  installPhase = ''
    mkdir -p $out/bin
    mv *.out $out/bin
  '';
}

