{ pkgs ? import <nixpkgs> {} } :

with pkgs; stdenv.mkDerivation {
  name = "cpp_randmat1";
  src = ./.;
  buildInputs = [
    cmake
    pkgconfig
    eigen3_3
  ];
}

