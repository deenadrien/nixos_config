with (import <nixos> {});

let
  _vim = import ./vim.nix { inherit pkgs; };
in

{
  allowUnfree = true;

  packageOverrides = pkgs: with pkgs; {

    myPackages = pkgs.buildEnv {
      name = "myPackages";
      paths = [

        # editeurs
        geany
        _vim aspellDicts.fr ctags
        vscode

        # linters
        hlint
        cppcheck
        python3Packages.flake8
        shellcheck
        proselint
        flow

        # haskell
        cabal-install
        ghc

        # divers dev
        gnumake
        nixops
        man-pages
        meld
        python3
        qtcreator
        sqlite
        sqlitebrowser
        tokei
        octave

        # bureautique
        chromium
        evince
        gimp
        gnome3.eog
        python3Packages.glances
        imagemagick
        libreoffice

      ];
    };
  };
}

